module gitlab.com/upchieve/two-am-takeout

go 1.17

require (
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/websocket v1.4.2
	github.com/stretchr/testify v1.7.0
)
